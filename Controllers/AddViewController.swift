//
//  AddViewController.swift
//  myExpenses
//
//  Created by Miguel Oliveira Santos on 06/04/2018.
import UIKit

class AddViewController: UIViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var expense:Expense?
    var index:Int?
    
    @IBOutlet weak var expenseNameField: UITextField!
    @IBOutlet weak var expenseValueField: UITextField!
    @IBOutlet weak var expenseDateField: UITextField!
    @IBOutlet weak var expenseLocationField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func addButtonPressed(_ sender: UIButton) {
        
        if expenseNameField.text != nil && expenseValueField.text != nil && expenseDateField != nil && expenseLocationField != nil{
            let expenseValueString = String(describing: expenseValueField.text!)
            let expenseValueFloat : Float = (expenseValueString  as NSString).floatValue //converte a string em float
            
            appDelegate.expenses.append(Expense(expenseName:"\(expenseNameField.text!)", expenseTag:"Tag", expenseValue:expenseValueFloat, expenseDate:"\(expenseDateField.text!)", expenseLocation:"\(expenseLocationField.text!)")) // adiciona uma expense
            _ = navigationController?.popViewController(animated: true) //Volta para a View anterior
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
