//
//  ExpensesDetailsViewController.swift
//  myExpenses
//
//  Created by Miguel Oliveira Santos on 27/04/2018.
//

import UIKit

class ExpensesDetailsViewController: UIViewController {
    
    var expense:Expense?
    var index:Int?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
   /* var expense = Expense(expenseName: "", expenseTag: "", expenseValue: 0, expenseDate: "", expenseLocation: "")*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let e = expense {
            nameLabel.text = e.expenseName
            valueLabel.text = String(e.expenseValue)
            dateLabel.text = e.expenseDate
            }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
