  //
//  ExpensesTableViewController.swift
//  myExpenses
//
//  Created by Miguel Oliveira Santos on 26/04/2018.
//

import UIKit

class ExpensesTableViewController: UITableViewController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var index = 0
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return appDelegate.expenses.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "expenseCell")
        cell.textLabel?.text = "\(appDelegate.expenses[indexPath.row].expenseName)" + " " + "\(appDelegate.expenses[indexPath.row].expenseValue)" + "€ " + "\(appDelegate.expenses[indexPath.row].expenseDate)"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let expense = appDelegate.expenses[indexPath.row]
        print(indexPath.row)
        print(expense.expenseName)
        index = indexPath.row
        self.performSegue(withIdentifier: "expenseDetailsSegue", sender: expense)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "expenseDetailsSegue" {
            if let controller = segue.destination as? ExpensesDetailsViewController {
                controller.expense = sender as? Expense
                controller.index = index
            }
        }
    }
}
