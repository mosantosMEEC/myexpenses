//
//  Expense.swift
//  myExpenses
//
//  Created by Miguel Oliveira Santos on 26/04/2018.
//

import Foundation
import CoreLocation

class Expense{
    var expenseName : String
    var expenseTag : String
    var expenseValue : Float
    //var expenseDate : Date
    //var expenseLocation : CLLocationCoordinate2D
    var expenseDate : String
    var expenseLocation : String
    
    //init(expenseName:String, expenseTag:String, expenseValue:Float, expenseDate:Date, expenseLocation:CLLocationCoordinate2D){
    init(expenseName:String, expenseTag:String, expenseValue:Float, expenseDate:String, expenseLocation:String){
        self.expenseName = expenseName
        self.expenseTag = expenseTag
        self.expenseValue = expenseValue
        self.expenseDate = expenseDate
        self.expenseLocation = expenseLocation
        
    }
    
}
